using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using OpenCvSharp;
using OpenCvSharp.Tracking;
using Intel.RealSense;
using UnityEngine.UI;
using Unity.Collections;
using System.Linq;
using UnityEditor;
using System.IO;


public class GameConfig : MonoBehaviour
{
    public Texture rgb { get; set; }
    public Texture depth { get; set; }
    public RawImage outputImage;
    private int downScale = 1;
    private Texture2D output;
    public static int distance = 0;
    public static Vector2 playerPosition = new Vector2();
    private int grouping = 2;
    private int padding = 2;

	private float xMinPlayer = 1000000;
	private float xMaxPlayer = 0;

	private float zMinPlayer = 1000000;
	private float zMaxPlayer = 0;

	private string square_instructuins = "\n\nKeep moving as long as there is a green square around your figure.\nWhen done, click <color=#BBD62C>Next</color>";
    private string[] instructuins = new string[] {"Move <color=#BBD62C>close</color> to the camera,\nand stand as <color=#BBD62C>far to the Left</color> as possible,",
												"Move <color=#BBD62C>far</color> from the camera,\nand stand as <color=#BBD62C> far to the Right</color> as possible,"};

    private string[] main_instructuins = new string[] {"Each time you will have to move\nto a different location.\nWhen you reach it, click <color=#BBD62C>Next</color>"};
    public Text instructionText;
    private int i = 0;
	Queue<float> queue = new Queue<float>();

	private void writeValuesToFile()
    {
		string path = Application.dataPath + "/boundaries.txt";

		File.WriteAllText(path, String.Empty);

		string minValues = xMinPlayer + "," + zMinPlayer + "\n";
		File.AppendAllText(path, minValues);

		string maxValues = xMaxPlayer + "," + zMaxPlayer + "\n";
		File.AppendAllText(path, maxValues);

		AEndMessage.boundExist = true;

	}
	
	private void setBoundaries(float xVal, float yVal)
    {
		if(xVal == 0 || yVal == 0 || yVal > 8000)
        {
			return;
        }

		if(xVal < xMinPlayer)
        {
			xMinPlayer = xVal;

		}
		else if (xVal > xMaxPlayer)
        {
			xMaxPlayer = xVal;
        }

		if (yVal < zMinPlayer)
		{
			zMinPlayer = yVal;
		}
		else if (yVal > zMaxPlayer)
		{
			zMaxPlayer = yVal;
		}
	}

	public void ConfigureGame()
    {
		if(i < 0)
        {
			i = 0;
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
		}
        else if(i >= main_instructuins.Length + instructuins.Length)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
			i = 0;
			writeValuesToFile();
		}
        else if (i < main_instructuins.Length)
        {
            instructionText.text = main_instructuins[i];
		}
        else
        {
            instructionText.text = instructuins[i- main_instructuins.Length] + square_instructuins;
		}

        i++;
    }

	public void back()
    {
		i -= 2;
		ConfigureGame();
	}

	// Start is called before the first frame update
	void Start()
    {
		Application.targetFrameRate = 30;
		//GameObject.Find("Panel").gameObject.SetActive(true); //   GetComponent<Image>().enabled = false;

		instructionText.text = main_instructuins[0];
        i++;
    }

	Vector2 ConvertToImageSpace(Vector2 coord, Size size)
	{
		var ri = outputImage;

		Vector2 output = new Vector2();
		RectTransformUtility.ScreenPointToLocalPointInRectangle(ri.rectTransform, coord, null, out output);

		// pivot is in the center of the rectTransform, we need { 0, 0 } origin
		output.x += size.Width / 2;
		output.y += size.Height / 2;

		// now our image might have various transformations of it's own
		//if (!TextureParameters.FlipVertically)
		//output.y = size.Height - output.y;

		// downscaling
		output.x *= downScale;
		output.y *= downScale;

		return output;
	}

	protected bool ProcessTexture(Texture2D rgb2d, NativeArray<ushort> depthArr)
	{

		Mat image = OpenCvSharp.Unity.TextureToMat(rgb2d);
		OpenCvSharp.Cv2.Flip(image, image, FlipMode.XY);
		Mat gray = new Mat();
		OpenCvSharp.Cv2.CvtColor(image, gray, ColorConversionCodes.RGB2GRAY);
		
		OpenCvSharp.HOGDescriptor hog = new OpenCvSharp.HOGDescriptor();
		hog.SetSVMDetector(OpenCvSharp.HOGDescriptor.GetDefaultPeopleDetector());

		// run the detector with default parameters. to get a higher hit-rate
		// (and more false alarms, respectively), decrease the hitThreshold and
		// groupThreshold (set groupThreshold to 0 to turn off the grouping completely).
		OpenCvSharp.Rect[] found = hog.DetectMultiScale(gray, out double[] weights, 0, new Size(8, 8), new Size(padding, padding), 1.02, grouping); // , new Size(24, 16), 1.05, 2);
																																					//Debug.Log(string.Join(",", weights.Select(f => f.ToString())));
		ushort dist = 0;
		//int xVal = 0;
		//ushort counter = 0;
		for (int i = 0; i < found.Length; i++)
		{
			OpenCvSharp.Rect rect = found[i];
			if (weights[i] < 1.5)
			{
				continue;
			}

			int idx = (rgb2d.height - rect.Center.Y) * rgb2d.width + (rgb2d.width - rect.Center.X);

			dist = depthArr[idx];
			if(dist == 0 || rect.Center.Y == 0 || rect.Center.X == 0)
            {
				continue;
            }
			var vec = new Intel.RealSense.Extensions.Vector2D();
			vec.X = rect.Center.X;
			vec.Y = rect.Center.Y;

			float projectX = Intel.RealSense.Extensions.CoordinateMapper.Map2DTo3D(DeprojectDepth.depth_intrinsics, vec, dist).X;
            //Debug.Log("ppx: " + DeprojectDepth.depth_intrinsics);
            //queue.Enqueue(dist);
            //if (queue.Count > 4)
            //    queue.Dequeue();

            //var fdist = queue.Where(d => d > 0).Where(d => d < 8000).Average();

            var fdist = dist;

			Debug.Log("------ before: " + rect.Center.X + ", after: " + projectX);

			setBoundaries(projectX, fdist);

			//dist += depthArr[idx];
			//xVal += rect.Center.X;
			//counter++;

			// the HOG detector returns slightly larger rectangles than the real objects.
			// so we slightly shrink the rectangles to get a nicer output.
			Rect2d obj = new Rect2d
			{
				X = rect.X + (int)Math.Round(rect.Width * 0.1),
				Y = rect.Y + (int)Math.Round(rect.Height * 0.1),
				Width = (int)Math.Round(rect.Width * 0.8),
				Height = (int)Math.Round(rect.Height * 0.8)
			};

			Vector2 startPoint = new Vector2((float)obj.TopLeft.X, (float)obj.TopLeft.Y);
			Vector2 endPoint = new Vector2((float)obj.BottomRight.X, (float)obj.BottomRight.Y);

			// screen space -> image space
			Vector2 sp = ConvertToImageSpace(startPoint, image.Size());
			Vector2 ep = ConvertToImageSpace(endPoint, image.Size());
			Point location = new Point(Math.Min(sp.x, ep.x), Math.Min(sp.y, ep.y));
			Size size = new Size(Math.Abs(ep.x - sp.x), Math.Abs(ep.y - sp.y));
			var areaRect = new OpenCvSharp.Rect(location, size);

			areaRect = new OpenCvSharp.Rect((int)obj.X, (int)obj.Y, (int)obj.Width, (int)obj.Height);

			Cv2.Rectangle((InputOutputArray)image, areaRect * (1.0 / downScale), Scalar.LightGreen);
		}

		//if(counter > 0)
  //      {
		//	dist = (ushort)(dist / counter);
		//	xVal = xVal / counter;

		//	queue.Enqueue(dist);
		//	if (queue.Count > 15)
		//		queue.Dequeue();

		//	var fdist = queue.Where(d => d > 0).Where(d => d < 8000).Average();
		//	setBoundaries(xVal, fdist);
		//	Debug.Log(fdist);
		//}

		// result, passing output texture as parameter allows to re-use it's buffer
		// should output texture be null a new texture will be created
		if (output == null)
			output = new Texture2D(rgb2d.width, rgb2d.height, rgb2d.format, false);

		OpenCvSharp.Unity.MatToTexture(image, output);
		outputImage.texture = output; // OpenCvSharp.Unity.MatToTexture(image, (Texture2D)GetComponent<RawImage>().texture);

		return true;
	}

	// Update is called once per frame
	void Update()
	{
		//GameObject.Find("Panel").GetComponent<Image>().enabled = false;

		Texture2D rgb2d = (Texture2D)rgb;
		Texture2D depth2d = (Texture2D)depth;
		//Debug.Log(rgb2d.width);

		var depthArr = depth2d.GetRawTextureData<ushort>(); // new Unity.Collections.NativeArray<ushort>(); //
														
		depth2d.GetRawTextureData();
		ProcessTexture(rgb2d, depthArr);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intel.RealSense;
using System.Linq;
using System;

public class DeprojectDepth : MonoBehaviour
{

    public RsFrameProvider Source;

    public static Intrinsics depth_intrinsics;
    //Intrinsics color_intrinsics;
    //Extrinsics color_to_depth_extrinsics;

    void Start()
    {
        Source.OnStart += OnStartStreaming;
    }

    private void OnStartStreaming(PipelineProfile obj)
    {
        using (var depth = obj.Streams.FirstOrDefault(s => s.Stream == Stream.Depth && s.Format == Format.Z16).As<VideoStreamProfile>())
        //using (var color = obj.Streams.FirstOrDefault(s => s.Stream == Stream.Color).As<VideoStreamProfile>())
            
        {
            depth_intrinsics = depth.GetIntrinsics();
        //    color_intrinsics = depth.GetIntrinsics();
        //    color_to_depth_extrinsics = color.GetExtrinsicsTo(depth);
        }
    }

    static Vector3 Deproject(int x, int y, float z, Intrinsics intrinsics)
    {
        return new Vector3
        {
            x = z * (x - intrinsics.ppx) / intrinsics.fx,
            y = z * (y - intrinsics.ppy) / intrinsics.fy,
            z = z
        };
    }

}

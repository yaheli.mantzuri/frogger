//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.EventSystems;
//using System;
//using OpenCvSharp;
//using OpenCvSharp.Tracking;
//using Intel.RealSense;
//using UnityEngine.UI;
//using Unity.Collections;
//using System.Linq;

//public class Configuration : MonoBehaviour
//{
//	public Texture rgb { get; set; }
//	public Texture depth { get; set; }
//	public RawImage outputImage;
//	private int downScale = 1;
//	private Texture2D output;
//	public static int distance = 0;
//	public static Vector2 playerPosition = new Vector2();
//	public int grouping;
//	public int padding;

//	public string[] instructuins = new string[]{"To start the configuration, click next", "great"}; 
//	public Text instructionText;
//	Size[] sizes = new Size[4];
//	int currentSizeIdx = 0;

//	void Start()
//	{
//		instructionText.text = instructuins[currentSizeIdx];
//		currentSizeIdx++;
//	}

//	public void SaveConfig()
//    {
//		Debug.Log(currentSizeIdx);
//		instructionText.text = instructuins[currentSizeIdx];
//		currentSizeIdx++;
//	}

//	Vector2 ConvertToImageSpace(Vector2 coord, Size size)
//	{
//		var ri = outputImage;

//		Vector2 output = new Vector2();
//		RectTransformUtility.ScreenPointToLocalPointInRectangle(ri.rectTransform, coord, null, out output);

//		// pivot is in the center of the rectTransform, we need { 0, 0 } origin
//		output.x += size.Width / 2;
//		output.y += size.Height / 2;

//		// now our image might have various transformations of it's own
//		//if (!TextureParameters.FlipVertically)
//		//output.y = size.Height - output.y;

//		// downscaling
//		output.x *= downScale;
//		output.y *= downScale;

//		return output;
//	}

//	protected bool ProcessTexture(Texture2D rgb2d, NativeArray<ushort> depthArr)
//	{
//		Mat image = OpenCvSharp.Unity.TextureToMat(rgb2d);
//		//OpenCvSharp.Cv2.Flip(image, image, FlipMode.Y);

//		Mat gray = new Mat();
//		OpenCvSharp.Cv2.CvtColor(image, gray, ColorConversionCodes.RGB2GRAY);

//		OpenCvSharp.HOGDescriptor hog = new OpenCvSharp.HOGDescriptor();
//		hog.SetSVMDetector(OpenCvSharp.HOGDescriptor.GetDefaultPeopleDetector());

//		bool b = hog.CheckDetectorSize();
//		//Console.WriteLine("CheckDetectorSize: {0}", b);

//		// run the detector with default parameters. to get a higher hit-rate
//		// (and more false alarms, respectively), decrease the hitThreshold and
//		// groupThreshold (set groupThreshold to 0 to turn off the grouping completely).
//		OpenCvSharp.Rect[] found = hog.DetectMultiScale(gray, out double[] weights, 0, new Size(8, 8), new Size(padding, padding), 1.02, grouping); // , new Size(24, 16), 1.05, 2);
//		//Debug.Log(string.Join(",", weights.Select(f => f.ToString())));

//		for (int i = 0; i < found.Length; i++)
//		//foreach (OpenCvSharp.Rect rect in found)
//		{
//			OpenCvSharp.Rect rect = found[i];
//			if (weights[i] < 1.5)
//			{
//				continue;
//			}
//			//Debug.Log(rect.Center);
//			ushort dist = depthArr[rect.Center.Y * rgb2d.width + rect.Center.X];
//			playerPosition.Set(rect.Center.X, dist);

//			//distance = dist;
//			//Debug.Log(dist);

//			// the HOG detector returns slightly larger rectangles than the real objects.
//			// so we slightly shrink the rectangles to get a nicer output.
//			Rect2d obj = new Rect2d
//			{
//				X = rect.X + (int)Math.Round(rect.Width * 0.1),
//				Y = rect.Y + (int)Math.Round(rect.Height * 0.1),
//				Width = (int)Math.Round(rect.Width * 0.8),
//				Height = (int)Math.Round(rect.Height * 0.8)
//			};

//			Vector2 startPoint = new Vector2((float)obj.TopLeft.X, (float)obj.TopLeft.Y);
//			Vector2 endPoint = new Vector2((float)obj.BottomRight.X, (float)obj.BottomRight.Y);

//			// screen space -> image space
//			Vector2 sp = ConvertToImageSpace(startPoint, image.Size());
//			Vector2 ep = ConvertToImageSpace(endPoint, image.Size());
//			Point location = new Point(Math.Min(sp.x, ep.x), Math.Min(sp.y, ep.y));
//			Size size = new Size(Math.Abs(ep.x - sp.x), Math.Abs(ep.y - sp.y));
//			var areaRect = new OpenCvSharp.Rect(location, size);

//			areaRect = new OpenCvSharp.Rect((int)obj.X, (int)obj.Y, (int)obj.Width, (int)obj.Height);

//			Cv2.Rectangle((InputOutputArray)image, areaRect * (1.0 / downScale), Scalar.LightGreen);
//		}

//		// result, passing output texture as parameter allows to re-use it's buffer
//		// should output texture be null a new texture will be created
//		if (output == null)
//			output = new Texture2D(rgb2d.width, rgb2d.height, rgb2d.format, false);

//		OpenCvSharp.Unity.MatToTexture(image, output);
//		outputImage.texture = output; // OpenCvSharp.Unity.MatToTexture(image, (Texture2D)GetComponent<RawImage>().texture);

//		return true;
//	}

//	private void processDepth(Texture2D texture)
//	{
//		byte[] input = texture.GetRawTextureData();
//		var input2 = texture.GetRawTextureData<ushort>();
//		//ushort dist = input[y * texture.width + x];
//		//create array of pixels from texture 
//		//remember to convert to texture2D first
//		//Color[] pixels = new Color[width * height];
//		Color[] pixels = new Color[texture.width * texture.height];

//		//converts R16 bytes to PNG32
//		for (int i = 0; i < input.Length; i += 2)
//		{
//			//combine bytes into 16bit number
//			UInt16 num = System.BitConverter.ToUInt16(input, i);
//			//turn into float with range 0->1
//			float greyValue = (float)num / 2048.0f;

//			float alpha = 1.0f;

//			//makes pixels outside measuring range invisible
//			if (num >= 2048 || num <= 0)
//				alpha = 0.0f;

//			Color grey = new Color(greyValue, greyValue, greyValue, alpha);

//			//set grey value of pixel based on float
//			pixels.SetValue(grey, i / 2);
//		}
//	}

//	// Update is called once per frame
//	void Update()
//	{
//		//Texture2D rgb2d = (Texture2D)rgb;
//		//Texture2D depth2d = (Texture2D)depth;
//		//Debug.Log(rgb2d.width);

//		//var depthArr = depth2d.GetRawTextureData<ushort>();
//		//processDepth(depth2d);

//		//depth2d.GetRawTextureData();
//		//ProcessTexture(rgb2d, depthArr);
//	}
//}

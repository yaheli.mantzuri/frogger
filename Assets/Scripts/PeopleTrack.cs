
namespace OpenCvSharp.Demo
{
	using UnityEngine;
	using UnityEngine.EventSystems;
	using System;
	using OpenCvSharp;
	using OpenCvSharp.Tracking;
    using System.Linq;

    /// <summary>
    /// Object tracking handler
    /// </summary>
    public class PeopleTrack: WebCamera
	{
		// downscaling const
		const float downScale = 1f;

		/// <summary>
		/// Initialization
		/// </summary>
		protected override void Awake()
		{
			base.Awake();
			forceFrontalCamera = true;
			DeviceName = WebCamTexture.devices[0].name; //"Intel(R) RealSense(TM) Depth Camera 415  rgb";
			
			Debug.Log(DeviceName);
		}

		/// <summary>
		/// Converts point from screen space into the image space
		/// </summary>
		/// <param name="coord"></param>
		/// <param name="size"></param>
		/// <returns></returns>
		Vector2 ConvertToImageSpace(Vector2 coord, Size size)
		{
			var ri = GetComponent<UnityEngine.UI.RawImage>();

			Vector2 output = new Vector2();
			RectTransformUtility.ScreenPointToLocalPointInRectangle(ri.rectTransform, coord, null, out output);

			// pivot is in the center of the rectTransform, we need { 0, 0 } origin
			output.x += size.Width / 2;
			output.y += size.Height / 2;

			// now our image might have various transformations of it's own
			if (!TextureParameters.FlipVertically)
				output.y = size.Height - output.y;

			// downscaling
			output.x *= downScale;
			output.y *= downScale;

			return output;
		}


		/// <summary>
		/// Our main function to process the tracking:
		/// 1. If there is no active dragging and no area - it does nothing useful, just renders the image
		/// 2. If there is active dragging - it renders image with dragging rectangle over it (red color)
		/// 3. if there is no dragging, but there is tracker and it has result - it draws image with tracked object rect over it (green color)
		/// </summary>
		/// <param name="input"></param>
		/// <param name="output"></param>
		/// <returns></returns>
		protected override bool ProcessTexture(WebCamTexture input, ref Texture2D output)
		{
			Mat image = Unity.TextureToMat(input, TextureParameters);

			OpenCvSharp.HOGDescriptor hog = new OpenCvSharp.HOGDescriptor();
			hog.SetSVMDetector(OpenCvSharp.HOGDescriptor.GetDefaultPeopleDetector());

			System.Diagnostics.Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();

			// run the detector with default parameters. to get a higher hit-rate
			// (and more false alarms, respectively), decrease the hitThreshold and
			// groupThreshold (set groupThreshold to 0 to turn off the grouping completely).
			OpenCvSharp.Rect[] found = hog.DetectMultiScale(image, out double[] weights, 0, new Size(8, 8), new Size(24, 16), 1.05, 2);

			Debug.Log(string.Join(",", weights.Select(f => f.ToString())));


			watch.Stop();

			foreach (OpenCvSharp.Rect rect in found)
			{
				Debug.Log(rect.Center);
				
				// the HOG detector returns slightly larger rectangles than the real objects.
				// so we slightly shrink the rectangles to get a nicer output.
				Rect2d obj = new Rect2d
				{
					X = rect.X + (int)Math.Round(rect.Width * 0.1),
					Y = rect.Y + (int)Math.Round(rect.Height * 0.1),
					Width = (int)Math.Round(rect.Width * 0.8),
					Height = (int)Math.Round(rect.Height * 0.8)
				};

				Vector2 startPoint = new Vector2((float)obj.TopLeft.X, (float)obj.TopLeft.Y);
				Vector2 endPoint = new Vector2((float)obj.BottomRight.X, (float)obj.BottomRight.Y);

				// screen space -> image space
				Vector2 sp = ConvertToImageSpace(startPoint, image.Size());
				Vector2 ep = ConvertToImageSpace(endPoint, image.Size());
				Point location = new Point(Math.Min(sp.x, ep.x), Math.Min(sp.y, ep.y));
				Size size = new Size(Math.Abs(ep.x - sp.x), Math.Abs(ep.y - sp.y));
				var areaRect = new OpenCvSharp.Rect(location, size);

				areaRect = new OpenCvSharp.Rect((int)obj.X, (int)obj.Y, (int)obj.Width, (int)obj.Height);

				Cv2.Rectangle((InputOutputArray)image, areaRect * (1.0 / downScale), Scalar.LightGreen);
			}


			// result, passing output texture as parameter allows to re-use it's buffer
			// should output texture be null a new texture will be created
			output = Unity.MatToTexture(image, output);
			return true;
		}
	}
}
using UnityEngine;

public class CarSpawner : MonoBehaviour {

	private float spawnDelay = 2f;
	private float curDelay;


	public GameObject car;

	public Transform[] spawnPoints;

	float nextTimeToSpawn = 0f;
	int maxCars = 10;
	int curCars = 0;


	void Update ()
	{
		curDelay = spawnDelay / Score.CurrentLevel;
		if (nextTimeToSpawn <= Time.time)
		{
			if(!AEndMessage.messageOn)
				SpawnCar();
			nextTimeToSpawn = Time.time + curDelay;
		}
	}

	void SpawnCar ()
	{
		int randomIndex = Random.Range(0, spawnPoints.Length);
		Transform spawnPoint = spawnPoints[randomIndex];

		Instantiate(car, spawnPoint.position, spawnPoint.rotation);
	}

}

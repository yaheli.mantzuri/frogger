using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	public static int CurrentScore = 0;
	public static int CurrentLevel = 1;

	public Text scoreText;
	public Text levelText;

	public static void initVars()
    {
		CurrentScore = 0;
		CurrentLevel = 1;
    }

	void Start ()
	{
		scoreText.text = CurrentScore.ToString();
		levelText.text = "Level: " + CurrentLevel.ToString();
	}

	private void Update()
    {
		scoreText.text = CurrentScore.ToString();
		levelText.text = "Level: " + CurrentLevel.ToString();
	}
}

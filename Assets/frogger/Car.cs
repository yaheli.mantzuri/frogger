using UnityEngine;
using UnityEngine.UI;

public class Car : MonoBehaviour {

	public Rigidbody2D rb;

	public float minSpeed = 8f;
	public float maxSpeed = 12f;
	
	float speed = 1f;


	void Start ()
	{
		speed = Score.CurrentLevel * 2;
		//speed = Random.Range(minSpeed, maxSpeed);
	}

	void FixedUpdate () {
		Vector2 forward = new Vector2(transform.right.x, transform.right.y);
        if (!AEndMessage.messageOn)
        {
			rb.MovePosition(rb.position + forward * Time.fixedDeltaTime * speed);
		}
	}

}

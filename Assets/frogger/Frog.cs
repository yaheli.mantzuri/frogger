using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Frog : MonoBehaviour {

	public Rigidbody2D rb;

	private float xMinFrog = -7;
	private float xMaxFrog = 7;

	private float zMinFrog = -4f;
	private float zMaxFrog = 5f;

	private float xMinPlayer = 0;
	private float xMaxPlayer = 640;

	private float zMinPlayer = 0;
	private float zMaxPlayer = 6000;

    private void Start()
    {
		string path = Application.dataPath + "/boundaries.txt";

		string[] boundaries = File.ReadAllLines(path);

		string[] topLeft = boundaries[0].Split(',');
		string[] bottomRight = boundaries[1].Split(',');

		xMinPlayer = float.Parse(topLeft[0]);
		zMinPlayer = float.Parse(topLeft[1]);

		xMaxPlayer = float.Parse(bottomRight[0]);
		zMaxPlayer = float.Parse(bottomRight[1]);

		Debug.Log("x min player" + xMinPlayer);
		Debug.Log("x max player" + xMaxPlayer);
		Debug.Log("z min player" + zMinPlayer);
		Debug.Log("z max player" + zMaxPlayer);
		//Debug.Log("ratio x: " + ((xMaxFrog - xMinFrog) / (xMaxPlayer - xMinPlayer)));
		//Debug.Log("ratio z: " + ((zMaxFrog - zMinFrog) / (zMaxPlayer - zMinPlayer)));
	}

	public Vector2 getPosition()
    {
		Vector2 playerPosition = RealSenseTrack.playerPosition;
		//Debug.Log("player position: " + playerPosition);

		playerPosition[0] = playerPosition[0] < xMaxPlayer ? playerPosition[0] : xMaxPlayer;
		playerPosition[0] = playerPosition[0] > xMinPlayer ? playerPosition[0] : xMinPlayer;

		playerPosition[1] = playerPosition[1] < zMaxPlayer ? playerPosition[1] : zMaxPlayer;
		playerPosition[1] = playerPosition[1] > zMinPlayer ? playerPosition[1] : zMinPlayer;

		playerPosition.Set(playerPosition[0] - xMinPlayer, zMaxPlayer - playerPosition[1]);
		
		float xPosition = playerPosition[0] * ((xMaxFrog - xMinFrog) / (xMaxPlayer - xMinPlayer));
		float zPosition = playerPosition[1] * ((zMaxFrog - zMinFrog) / (zMaxPlayer - zMinPlayer));

		zPosition = zPosition < 0 ? 0 : zPosition;

		Vector2 newPosition = new Vector2(xMinFrog + xPosition, zMinFrog + zPosition > 4 ? 4 : zMinFrog + zPosition);
		//Debug.Log("new position: " + newPosition);
		return newPosition;
	}

	void Update () {
		rb.MovePosition(getPosition());

        Debug.Log(rb.position);
        //if (Input.GetKeyDown(KeyCode.RightArrow))
        //    rb.MovePosition(rb.position + Vector2.right);
        //else if (Input.GetKeyDown(KeyCode.LeftArrow))
        //    rb.MovePosition(rb.position + Vector2.left);
        //else if (Input.GetKeyDown(KeyCode.UpArrow))
        //    rb.MovePosition(rb.position + Vector2.up);
        //else if (Input.GetKeyDown(KeyCode.DownArrow))
        //    rb.MovePosition(rb.position + Vector2.down);
    }

    void OnTriggerEnter2D (Collider2D col)
	{
		
		if (col.tag == "Car")
		{
            if (!AEndMessage.messageOn)
            {
				AEndMessage.didLost = true;
				Score.CurrentScore -= 50;
			}
			
		}
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Intel.RealSense;
using System.IO;

public class AEndMessage : MonoBehaviour
{
    public Text status;
    public Text instructions;
    public Text timer;
    private string instructions_content = "If you wish to play, go to the yellow line";

    public static bool firstRun = true;
    public static bool atStart = false;
    public static bool didWin = false;
    public static bool didLost = false;
    public static bool messageOn = false;

    private int reset_max_seconds = 10;
    private int bound_max_seconds = 5;

    private System.DateTime time = System.DateTime.Now;
    public static bool boundExist = false;

    public static void initVars()
    {
        firstRun = true;
        atStart = false;
        didWin = false;
        didLost = false;
        messageOn = false;
}

    // Start is called before the first frame update
    void Start()
    {
        ResetAll();
        boundExist = File.Exists(Application.dataPath + "/boundaries.txt");
    }

    // Update is called once per frame
    void ResetAll()
    {
        GameObject.Find("Panel").gameObject.GetComponent<Image>().enabled = false;
        didLost = false;
        didWin = false;
        atStart = false;
        status.text = "";
        instructions.text = "";
        timer.text = "";
    }
    
    void Update()
    {
        messageOn = GameObject.Find("Panel").gameObject.GetComponent<Image>().enabled;

        if (GameObject.Find("Panel").gameObject.GetComponent<Image>().enabled)
        {
            int max_seconds;
            if (!boundExist)
            {
                instructions.text = "You need to first configure the game";
                max_seconds = bound_max_seconds;
            }
            else
            {
                instructions.text = instructions_content;
                max_seconds = reset_max_seconds;
            }

            System.DateTime now = System.DateTime.Now;
            int sec = (System.DateTime.Now - time).Seconds;
            int seconds = max_seconds - (System.DateTime.Now - time).Seconds;
            
            timer.text = "Closing in " + seconds.ToString() + " seconds";

            if (atStart)
            {
                ResetAll();
            }
            else if (seconds <= 0)
            {
                ResetAll();
                Score.CurrentScore = 0;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
            }
        }

        else if (didLost || didWin || !boundExist || firstRun)
        {
            if (!boundExist)
            {
                status.text = "<color=#FF0000>No Configurations!</color>";
            }
            else if(firstRun) 
            {
                status.text = "<color=#BBD62C>Start The Game</color>";
            }

            else if (didWin)
            {
                status.text = "<color=#BBD62C>You Won!</color>";
            }
            else
            {
                status.text = "<color=#FF0000>You Lost!</color>";
            }

            time = System.DateTime.Now;
            GameObject.Find("Panel").gameObject.GetComponent<Image>().enabled = true;
            didLost = false;
            didWin = false;
            atStart = false;
            firstRun = false;
        }
    }
}
